package com.lsusr.emojis.emojis

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.widget.Button
import android.widget.Toast


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun copyText(text: String) {
        val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("text label", text)
        clipboard.setPrimaryClip(clip)
        Toast.makeText(this, "Copied \"" + text + "\" to clipboard", Toast.LENGTH_SHORT).show()
    }

    fun click(view: View) {
        val button: Button = view as Button
        val text = button.text as String
        copyText(text)
    }
}
